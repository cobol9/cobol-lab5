       IDENTIFICATION DIVISION. 
       PROGRAM-ID. STAR-100.
       AUTHOR. BENJAMAS.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 

       PROCEDURE DIVISION .
       000-BEGIN.
           PERFORM 10 TIMES 
              PERFORM 001-PRINT-STAR-OUTLINE THRU 001-EXIT 
           END-PERFORM 
           GOBACK
       .
       
   
       001-PRINT-STAR-OUTLINE. 
           PERFORM 002-PRINT-ONE_STAR 10 TIMES
           DISPLAY ""
       .

       001-EXIT.
           EXIT 
       .

       002-PRINT-ONE_STAR.
           DISPLAY "*" WITH NO ADVANCING 
       .