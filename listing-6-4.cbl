       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-6-4.
       AUTHOR. BENJAMAS.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 COUNTERS.
           02 HUNDREDS-COUNT PIC 99 VALUE ZEROS .
           02 TENS-COUNT PIC 99 VALUE ZEROS .
           02 UNITS-COUNT PIC 99 VALUE ZEROS .

       01 ODONETER.
           02 PRN-HUNDREDS PIC 9.
           02 FILLER PIC X VALUE "-".
           02 PRN-TENS PIC 9.
           02 FILLER PIC X VALUE "-".
           02 PRN-UNITS PIC 9.

       PROCEDURE DIVISION.
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform "
           PERFORM 001-COUNT-MILEAGE VARYING 
              HUNDREDS-COUNT FROM 0 BY 1 UNTIL HUNDREDS-COUNT > 9 
              AFTER TENS-COUNT FROM 0 BY 1 UNTIL TENS-COUNT > 9
              AFTER UNITS-COUNT FROM 0 BY 1 UNTIL UNITS-COUNT > 9

      *     DISPLAY "Now using in-line Perform"
      *    PERFORM VARYING 
      *         HUNDREDS-COUNT FROM 0 BY 1 UNTIL HUNDREDS-COUNT > 9

      *         PERFORM VARYING 
      *           TENS-COUNT FROM 0 BY 1 UNTIL TENS-COUNT > 9

      *           PERFORM VARYING 
      *              UNITS-COUNT FROM 0 BY 1 UNTIL UNITS-COUNT > 9
                    
      *              MOVE HUNDREDS-COUNT TO PRN-HUNDREDS 
      *              MOVE TENS-COUNT TO PRN-TENS 
      *              MOVE UNITS-COUNT TO PRN-UNITS 
      *           END-PERFORM
      *         END-PERFORM
      *     END-PERFORM

           DISPLAY "End of odometer simulation."
           GOBACK 
           .

       001-COUNT-MILEAGE.
           MOVE HUNDREDS-COUNT TO PRN-HUNDREDS  
           MOVE TENS-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNITS 
           DISPLAY "Out - " ODONETER 
           .