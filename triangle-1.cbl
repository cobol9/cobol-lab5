       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  triangle-1.
       AUTHOR. BENJAMAS.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE PIC X(80) VALUE SPACES .
       01 STAR-NUM PIC 999 VALUE ZEROS .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01 INDEX-NUM PIC 999 VALUE ZEROS .

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT 
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT 
              VARYING INDEX-NUM FROM 1 BY 1 
              UNTIL INDEX-NUM > STAR-NUM 
           GOBACK 
       .

       001-PRINT-STAR-LINE.
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM) 
           DISPLAY SCR-LINE 
       .

       001-EXIT.
           EXIT.

       002-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
              DISPLAY "Pleast input star number : " WITH NO ADVANCING 
              ACCEPT STAR-NUM

              IF NOT VALID-STAR-NUM 
                 DISPLAY "Please input star number in possitive : "
              END-IF 
           END-PERFORM
           
       .

       002-EXIT.
           EXIT
       .