       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  triangle-4.
       AUTHOR. BENJAMAS.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE PIC X(80) VALUE SPACES .
       01 STAR-NUM PIC 999 VALUE ZEROS .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01 INDEX-NUM1 PIC 999 VALUE ZEROS .
       01 INDEX-NUM2 PIC 999 VALUE ZEROS .

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM 001-INPUT-STAR-NUM THRU 001-EXIT 
           PERFORM VARYING INDEX-NUM1 FROM STAR-NUM  BY -1 
              UNTIL INDEX-NUM1 = 0

              MOVE ALL SPACES TO SCR-LINE
              COMPUTE INDEX-NUM2 = STAR-NUM - INDEX-NUM1 + 1 
              
              MOVE ALL"*" TO SCR-LINE(INDEX-NUM2  : INDEX-NUM1 )
              DISPLAY SCR-LINE 
           END-PERFORM

           GOBACK 
       .

       001-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
              DISPLAY "Pleast input star number : " WITH NO ADVANCING 
              ACCEPT STAR-NUM

              IF NOT VALID-STAR-NUM 
                 DISPLAY "Please input star number in possitive : "
              END-IF 
           END-PERFORM
           
       .

       001-EXIT.
           EXIT
       .
