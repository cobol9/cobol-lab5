       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-6-3.
       AUTHOR. BENJAMAS.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 REP-COUNT PIC 9(4).
       01 PRINT-REP-COUNT PIC Z,ZZ9.
       01 NUMBER-OF-TIMES PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM VARYING REP-COUNT FROM 0 BY 50 
              UNTIL REP-COUNT = NUMBER-OF-TIMES 

              MOVE REP-COUNT TO PRINT-REP-COUNT
              DISPLAY "Counting " PRINT-REP-COUNT 
           END-PERFORM

           MOVE REP-COUNT TO PRINT-REP-COUNT 
           DISPLAY "If I have told you once, I've told you " 
           PRINT-REP-COUNT " times." 
           
           GOBACK 

       .